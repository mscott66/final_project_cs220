#include "DiscardPile.h"
#include "Card.h"
#include <vector>
using std::vector;

void DiscardPile::add12(vector<Card> cards) {
    for(int i = 0; i < 12; i++) {
      Card& temp = cards[i];
      addCard(temp);
    }
  }

vector<Card> DiscardPile::reuse(){
  vector<Card> temp;
  while(!pile.empty()){
    temp.push_back(pile.front());
    pile.erase(pile.begin());
  }
  return temp;
}




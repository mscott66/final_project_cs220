#include <iostream>
#include <string>
#include <sstream>
#include "Player.h"
#include "Hand.h"
using std::cout;
using std::endl;
using std::string;

// for live game play - must not change!
void Player::display() {
  std::cout << "Current ";
  std::cout << name << ":  Stock{0}: " ;
  std::string temp = stock->toString();
  std::cout << "[" << temp[2] << "]/" << temp[0];
  std::cout << std::endl;
  std::cout << "   Discards{1-4}: ";
  for (int i = 0; i < 4; ++i) {
    temp = discard[i].toString();
    std::cout << "--/" << temp[0]; 
    std::cout << "  ";
  }
  std::cout << std::endl;
  std::cout << "   Hand{5-9}: ";
  std::cout << hand->toString();
  std::cout << std::endl;
}

/* saving state format - must not change!
PlayerName
Stock size
01 02 03  ...
...
Hand size
01 02 03 04 05
Discard1 size
01 02 03 04 ...
...
Discard2 size
...
Discard3 size
...
Discard4 size
...
*/
std::string Player::toString() const {
  std::stringstream result;
  result << name << "\n";
  result << "Stock " << stock->toString();
  result << "Hand " << hand->toString();
  for (int i = 0; i < 4; ++i) {
    result << "Discard" << i << " " << discard[i].toString();
  }
  return result.str();
}
  
bool Player::checkValidMove(char playFrom, char playTo, BuildPile build[4]){

  int validLocations = 0; //will be set to -1 if error, 0 if nothing happens, 1 if playing to buildpile, 2 if playing to discardpile
  
  //first check if we are calling from valid location
  if(playFrom >= '0' && playFrom <= '4') //if playing from stock or discard 
    { //can only play to buildpile
      if(playTo >= 'a' && playTo <= 'd'){
	validLocations = 1;
      }
      else{
	validLocations = -1;
      }
    }
  else if(playFrom >= '5' && playFrom <= '9') //if playing from hand 
    { //can play to buildpile or discard pile
      if(playTo >= 'a' && playTo <= 'd')
	{
	  validLocations = 1;
	}
      else if(playTo >= '1' && playTo <= '4'){
	  validLocations = 2;
	}
      else{ 
	  validLocations = -1;
        }
    }
  //else false because no other options that make sense
  else{
      validLocations = -1;
    }

  //error check: build command doesnt work
  if(validLocations<=0){
    cout << "Command is out of scope. Returning error" << validLocations << endl;
    return false;
  }
  //if valid locations is 2(hand to discardpile) then definitely allowed (all cards from hand can be put to all discardpiles)
  if(validLocations == 2){return true;}
  //else valid locations will be 1(we are moving to build pile). Now we check if card being moved's value = card value on top of pile + 1 (size of pile + 1)
  int value;
  if(playFrom == '5') {Card* c = getCardP(0);
    value = c->getValue();}
  else if(playFrom == '6') {Card* c = getCardP(1);
  value = c->getValue();}
  else if(playFrom == '7') {Card* c = getCardP(2);
  value = c->getValue();}
  else if(playFrom == '8') {Card* c = getCardP(3);
  value = c->getValue();}
  else if(playFrom == '9') {Card* c = getCardP(4);
  value = c->getValue();}
  else if(playFrom == '1') {Card* c = &discard[0].showTop();
  value = c->getValue();}
  else if(playFrom == '2') {Card* c = &discard[1].showTop();
  value = c->getValue();}
  else if(playFrom == '3') {Card* c = &discard[2].showTop();
  value = c->getValue();}
  else if(playFrom == '4') {Card* c = &discard[3].showTop();
  value = c->getValue();}
 
  //wild card can always be played
  if (value == 0){
    return true;
    //valid move is card to anywhere of the 4
  }
  else {
    int less = value - 1;
    //read in which build pile we building from.
    BuildPile * b;
    if(playTo == 'a'){b = &build[0];}
    else if(playTo == 'b') {b = &build[1];}
    else if(playTo == 'c') {b = &build[2];}
    else if(playTo == 'd') {b = &build[3];}

    //if buildldpile size is value - 1 then we can make the move
    if(b->size() == less){
      return true; 
    }
  }
  return false; 
}

//function to call checkValidMove on all possibilities
bool Player::anyPossibleMoves(BuildPile build[4]) {
  return true;

  int count = 0;
  //play from: 0-9, play to: 1-4
  for(int i = 0; i < 10; i++){
    for(int j = 1; j < 5; j++){
      char playFrom = '0'+i;
      char playTo = '0'+j;
      if(checkValidMove(playFrom,playTo,build)){
	count++;
      }
    }
  }
  //play from: 0-9, play to: a-d
  for(int i = 0; i < 10; i++){
    for(int j = 0; j < 4; j++){
      char playFrom = '0'+i;
      char playTo = 'a'+j;
      if(checkValidMove(playFrom,playTo,build)){
        count++;
      }
    }
  }
  if(count==0) {return false;}
  else{return true;}
}

void Player::Move(char playFrom, char playTo, BuildPile build[4]){
  //account for ASCII value change
  int iplayFrom = playFrom - '0';
  int iplayTo;
  if(playTo>='a'&&playTo<='d'){
    iplayTo = playTo - 97;
  }
  else{iplayTo = playTo - '1';}
  //check if the pile to play to is the discards
  if (playTo >= '1' && playTo <= '4') {    
      //move from hand to desired discard
      discard[iplayTo].addCard(hand->removeFromHand(5-iplayFrom));      
  }
  else{
  //if the pile to play to is valid
  if (iplayTo >= 0 && iplayTo <= 3) {
    if (iplayFrom == 0) {
      //move from stock to build
      build[iplayTo].addCard(stock->drawCard());
    }
    else if (iplayFrom >= 1 && iplayFrom <= 4) {
      //move from discard to build
      //-------------------------------------------------
      build[iplayTo].addCard(discard[iplayFrom].drawCard());
    }
    else if (iplayFrom >= 5 && iplayFrom <= 9) {
      //move from hand to build
      build[iplayTo].addCard(hand->removeFromHand(5-iplayFrom));
    }
  }
  }
}

/*
.h file for the game board
*/

#ifndef SKIPBOGAME_H
#define SKIPBOGAME_H
#include <stdio.h>
#include "Player.h"
#include "DrawPile.h"
#include "BuildPile.h"
#include "BuildPile.h"
#include "DiscardPile.h"
#include <array>

class SkipBoGame {

 private:
  std::vector<Player> *players;
  int num_players;
  DrawPile* draw;
  BuildPile build[4];
  DiscardPile* discard;
  //  int stockSize;
  int curp;
  
 public:
 SkipBoGame(std::vector<Player> *players, DrawPile* draw,		   \
	    BuildPile builds[4], int curp) :				\
  players(players), draw(draw), curp(curp){
    num_players = players->size();

    DiscardPile temp = DiscardPile();
    discard = &temp;
    for(int i = 0; i < 4; i++){
      build[i] = builds[i];
    }
  }

  //ADD IMPLEMENTATION FOR SHUFFLE IS TRUE/FALSE

  //Deals to player a
  void deal();
  Player* play();

  void display();
  std::string toString() const;

  Player getPlayer(int a) {
    return players->at(a);
  }

};

#endif

#ifndef PLAYPILE_H
#define PLAYPILE_H

#include "FaceUpPile.h"
#include <vector>
#include <stdio.h>
using std::cout;

class PlayPile : public FaceUpPile {
 public:
 PlayPile() : FaceUpPile() { }
 PlayPile(std::vector<Card> v) : FaceUpPile() {
    setPile(v);
  }

  void display() override {}
  
  
};

#endif

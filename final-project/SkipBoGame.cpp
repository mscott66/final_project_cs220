#include <iostream>
#include <sstream>
#include <string>
#include <vector>
#include <fstream>
#include "SkipBoGame.h"
using std::cout;
using std::endl;
using std::string;
using std::vector;
using std::ofstream;


/* for live game play - must not change format!

drawPile  build_a  build_b  build_c  build_d
playerName  stock_0  
discards: discard_1 discard_2 discard_3 discard_4
hand: card_5 card_6 card_7 card_8 card_9
 */
void SkipBoGame::display() {
  std::cout << "Draw: ";
  draw->display();
  std::cout << "  Build Piles: ";
  for (int j = 0; j < 4; j++) {
    build[j].display();
    std::cout << " ";
  }
  std::cout << std::endl;
  ((*players).at(curp)).display();
}

/* for saving state - must not change format!

shuffle numplayers currplayer
PlayerCurp [display]
PlayerCurp+1 [display]
[etc for all players]
Draw [display]
Build_a [display]
Build_b [display]
Build_c [display]
Build_d [display]
*/
std::string SkipBoGame::toString() const {
  std::stringstream result;
  int idx;
  result << draw->getRand() << " " << num_players << " " << curp << "\n";
  for (int i = 0; i < num_players; ++i) {
    idx = (curp+i) % num_players;
    result << (*players)[idx].toString();
  }
  result << "Draw " << draw->toString(); 
  for (int j = 0; j < 4; j++) {
    result << "Build_" << char('a'+j) << " ";
    result << build[j].toString();  
  }
  return result.str();
}

Player* SkipBoGame::play() {

  if(players->size() == 0) {

    return NULL;
  }
  int i = 0;


  Player& curr = (*players)[i];
  std::vector<char> moves;

  char options;
  bool won = false;
  while(!won) {
    Player curr = getPlayer(i);
    std::cout << endl << "Player" << i << " turn next" << endl;
    std::cout << "(p)lay, (s)ave, or (q)uit ?";
    std::cin >> options;

    //This will quit the game, returning NULL for the game and not saving
    if (options == 'q') {
      std::cout << "thanks for playing" << endl;
      return NULL;

  //This code will write the state to a save file
    } else if (options == 's') {
      std::cout << "save filename: ";
      std::string filename;
      std::cin >> filename;
      std::stringstream output;
      output << draw->getRand() << " " << num_players \
	     << " " <<players->at(0).handSize() << endl;
      for (int i = 0; (unsigned)i < (unsigned)num_players; i++) {
	curr = getPlayer(i);
	output << curr.toString();
      }
      output << this->toString();

      ofstream savefile;
      savefile.open(filename);
      savefile <<  output.str();
      return NULL;
    }

      else if (options != 'p') {
	std::cout << "invalid command, try again" << endl;
	continue;
     
      
    //This will run the game as normal 
    } else {

      bool notHandToDiscard = true;
      while (notHandToDiscard) {
	deal();
	this->display();
	//replace with the fucntion that checks if there are any
	//possible moves
	
	  std::cout << "(m)ove [start] [end] or (d)raw ? ";
	  std::cin >> options;
	  if (options == 'm') {
	    char start, end;
	    std::cin >> start;
	    std::cin >> end;
	    if (!curr.checkValidMove(start,end, build)) {
	      std::cout << "illegal command, try again" << endl << endl;
	      continue;
	    }
	    if(start <= '9' && start >= '5' && end <= '4' && end >= '1'){
	      notHandToDiscard = false;
	    }
	    curr.Move(start,end, build);
	  } else if (options == 'd') {
	    //calls deal function on current player
	    deal();
	  } else {
	    std::cout << "illegal command, try again" << endl << endl;
	    continue;
	  }
	
	won = (curr.stock_length() == 0);
      }
      }
    if (i == num_players - 1) {
      i = 0;
    } else {
      i++;
    }
  }
  return &curr;
}

void SkipBoGame::deal(){
  Player curr = getPlayer(curp);
  //not sure if below correct: draw() function in DrawPile, can it be accessed?
  while(curr.handSize()<5){
    curr.getHand()->addCard(draw->drawCard());
  }
}


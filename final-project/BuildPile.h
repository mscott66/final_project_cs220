#ifndef BUILDPILE_H
#define BUILDPILE_H
#include "Pile.h"
#include "FaceUpPile.h"
#include <vector>
#include "Card.h"
#include <iostream>
#include <string>
#include "DiscardPile.h"

class BuildPile : public FaceUpPile { 
 public:
  void send12(DiscardPile* d); //function that sends stack of 12 cards in build pile to discard pile
 BuildPile() : FaceUpPile(){} //constructor
 BuildPile(std::vector<Card> c) : FaceUpPile() {
    setPile(c);
  }

  void display();
};

#endif

#ifndef STOCKPILE_H
#define STOCKPILE_H

#include "Card.h"
#include "FaceUpPile.h"
#include "PlayPile.h"
#include <stdio.h>


class StockPile : public PlayPile{
 private:
  int stockSize;

 public:
   StockPile(int stockSize) : PlayPile(), stockSize(stockSize) { }
  //stockpile from savestate
  /*StockPile(std::vector<Card> savedStock): PlayPile(), stockSize(savedStock.size()){ }*/
  int getLength() { return pile.size(); }

  void display() const {}
  
};

#endif

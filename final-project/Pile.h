#ifndef _PILE_H
#define _PILE_H

#include <vector>
#include <iostream>
#include "Card.h"

class Pile {
 protected:
  std::vector<Card> pile;

 public:
  Pile() { }

  int size() const { return pile.size(); }

  virtual void addCard(const Card& c) { pile.push_back(c); }
  
  std::string toString() const;  // for saving state

  virtual void display() = 0;  // for live game play, must override

  Card drawCard();

  Card removeFromHand(int i);
  
  Card& showTop() {return *pile.begin();}

  void setPile(std::vector<Card> c) {
    pile = c;
  }

  Card& at(int a) {
    return pile.at(a);
  }

  Card* pat(int a) {
    return &pile.at(a);
  }

};
#endif

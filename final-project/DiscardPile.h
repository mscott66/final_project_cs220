#ifndef DISCARD_H
#define DISCARD_H
#include "Card.h"
#include <vector>
#include "FaceUpPile.h"
#include <stdio.h>

class DiscardPile : public FaceUpPile {
 public:
 DiscardPile() : FaceUpPile() { }
  
  void add12(std::vector<Card> cards);

  void display() { }
  std::vector<Card> reuse();
};
  
#endif
  

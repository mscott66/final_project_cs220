#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <stdio.h>
#include "Card.h"
#include "DrawPile.h"
#include "BuildPile.h"
#include "DiscardPile.h"
#include "FaceUpPile.h"
#include "Hand.h"
#include "Player.h"
#include "Pile.h"
#include "PlayPile.h"
#include "SkipBoGame.h"
#include "StockPile.h"
#include <sstream>


using std::cin;
using std::cout;
using std::endl;
using std::string;
using std::vector;

int runFromStart(char*argv[]);
int runFromSave(char*argv[]); 

int main(int argc, char* argv[]){
  //if new game
  if(argc == 5) {
    return runFromStart(argv);
  }
  //if loading from save
  if(argc == 3) {
    return runFromSave(argv);
    }
  cout <<"Error; incorrect call to function" <<endl;
  return 0;
}


int runFromStart(char*argv[]) {
//initialize draw pile
  
  std::ifstream file(argv[4]);
  //need to add error if file is not open
  int cardNum;
  vector<Card> deck;
  while(file >> cardNum){
    Card c = Card(cardNum);
    deck.push_back(c);
  }
  DrawPile draw;
  draw.setPile(draw.add(deck,argv[1]));
  
  //initialize empty buildpile and discardpile
  BuildPile builds[4];
  for(int i = 0; i < 4; i++) {
    BuildPile b = BuildPile();
    builds[i]= b;
  }

  PlayPile plays[4];
  
  std::stringstream s;
  for(int i = 0; argv[3][i] != 0; i++){
    char c = argv[3][i];
    s << c;
  }
  string stock = s.str();
  int stockSize = std::stoi(stock);

  //initialize players
  int numPlayers = (int)*argv[2] - 48;
  std::cout << "num players is " << numPlayers << std::endl;
  std::cout << "stock size is " << stockSize << std::endl;
  vector<Player> players;
  
  for(int i = numPlayers; i > 0; i--){
    std::stringstream name;
    name << "Player" << (numPlayers - i);
    string playerName = name.str();

    for(int j = 0; j < 4; j++) {
      PlayPile b = PlayPile();
      plays[j] = b;
    }

    StockPile s = StockPile(stockSize);
    for(int i = 0; i < stockSize; i++){
      s.addCard(draw.drawCard());
    }
    Hand h = Hand();
    /*for(int i = 0; i < 5; i++){
      h.addCard(draw.drawCard());
      }*/
    Player p = Player(playerName, plays, &s, &h); //play pile of player 0 is first play pile in plays vector, player 1 is second pile in vector, etc  
    players.push_back(p);
  }
  

  //initialize game with parameters: array of players, num players, stock size, draw/build/discard pile
  SkipBoGame game = SkipBoGame(&players, &draw, \
				    builds, 0);

  game.play();
  return 1;
}


int runFromSave(char * argv[]) {
  string waste = NULL;
  std::ifstream file(argv[4]);

  //  FILE* in;
  /*
    if(!(in = fopen(argv[2], "r"))) {
	return 1;
    }
  */


  string s = NULL;
  file >> s;
  if(s.empty() ) {
    return 1;
  }


  string shuff = NULL;
    file >> shuff;
  if(shuff.empty()) { return 1; }

  
    int numPlayers = -1, curp = -1;
    file >> numPlayers >> curp;
    if(numPlayers == -1 || curp == -1) { return 1; }

    //make player array
    vector<Player> players;


    //for every player
    for(int i = 0; i < numPlayers; i++) {
      string name = NULL;
      file >> name;
      if(name.empty()) { return 1; }
      

      //how many cards in the player's stock
      int stockSize = -1;
      file >> waste;
      file >>stockSize;
      if(stockSize == -1) { return 1; }

      
      vector<Card> stockCards;
      for(int j = 0; j < stockSize; j++) {
	int temp;
	file >> temp;
	Card c = Card(temp);
	stockCards.push_back(c);
      }
      StockPile s = StockPile(stockSize);
      s.setPile(stockCards);
      
      int handSize; 
      vector<Card> handCards;
      file >>handSize;
     
      for(int j = 0; j < handSize; j++) {
	int temp;
	file >> temp;
	Card c = Card(temp);
	handCards.push_back(c); 
      }
      Hand h = Hand();
      h.setPile(handCards);
      
      PlayPile play[4];
      for(int j = 0; j < 4; j++) {
	int test = 0, num = 0;
	file >> waste;
	file >> test;
	file >> num;

	vector<Card> pCards;
	for(int k = 0; k < num; k++) {
	  int temp;
	  file >> temp;
	  Card c = Card(temp);
	  pCards.push_back(c);
	}
	PlayPile b = PlayPile(pCards);
	play[j] = b;
      }
      
      Player p = Player(string(name), play, &s, &h);
      players.push_back(p);
    }

    //make the deck
    vector<Card> deck;
    int temp;
    file >> waste;
    file >> temp;
   
    for(int i = 0; i < temp; i++) {
      int t;
      file >> t;
      Card c = Card(t);
      deck.push_back(c);
    }
    DrawPile dp;
    dp.setPile(deck);

//make the build piles lol idk why formatting is so off yikessssss
BuildPile buildPiles[4];
for(int i = 0; i < 4; i++){
  string ignore;
  vector<Card> buildDeck;
  file >> ignore;
  file >> temp;
  
  
  //for each build pile, read in the correct no of cards
  for(int j = 0; j < temp; j++) {
    int t;
    file >>t;
   
    Card c = Card(t);
    buildDeck.push_back(c);
  }
  BuildPile bp = BuildPile(buildDeck);
  buildPiles[i] = bp;
 }

SkipBoGame game = SkipBoGame(&players, &dp, buildPiles, curp);

game.play();
return 1;

  }
 

    

      

    
      
      
    











    

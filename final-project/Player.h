#ifndef PLAYER_H
#define PLAYER_H

#include "Hand.h"
#include <string.h>
#include "StockPile.h"
#include "PlayPile.h"
#include "BuildPile.h"

class Player {
 private:
  std::string name;
  PlayPile  discard[4];
  StockPile* stock;
  Hand* hand;
  int points;
  
 public:
  Player() { } 
 Player(std::string name, PlayPile pp[4], StockPile* s, Hand* h) :	\
  name(name), stock(s), hand(h){
    for(int i = 0; i < 4; i++){
      discard[i] = pp[i];
    }
  }

  int stock_length() {
    return stock->getLength();
  }
  
  bool checkValidMove(char playFrom, char playTo, BuildPile build[4]);
  //return the possible moves for the player
  //std::vector<char> getOptions();

  //calls checkValidMove to see if there are ANY possible moves
  bool anyPossibleMoves(BuildPile build[4]);
  //display to cout the player stuff

  void display(); 

  std::string toString() const;

  Card& getHandAt(int a) {
    Card * temp = hand->pat(a);
    return * temp;
  }
    

  Card* getCardP(int a) {
    Card& temp = getHandAt(a);
    Card * pt = &temp;
    return pt;
  }

  int handSize() {
    return hand->size();
  }

  Hand* getHand() {
    return hand;
  }
  
  void Move(char playFrom, char playTo, BuildPile build[4]);
  

};


#endif

#include <iostream>
#include "DrawPile.h"
#include <vector> 
#include <algorithm>
#include <cstdlib>

// for live game play - must not change!
void DrawPile::display() {
  std::cout << "[XX]/" << size();
}

/*for DrawPile shuffle, takes vector of cards and boolean, with boolean
  determining whether cards are shuffled or kept in order (true = shuffle)*/

std::vector<Card> DrawPile::add(std::vector<Card> cards,bool trueOrFalse){
  Rand = trueOrFalse;
  if(trueOrFalse){
    return shuffle(cards);
  }
  else{return cards;}
}

std::vector<Card> DrawPile::shuffle(std::vector<Card> cards){
  std::random_shuffle(cards.begin(),cards.end());
  return cards;
}

/*Card DrawPile::draw(){
  Card temp = this->pile.front();
  this->pile.erase(pile.begin());
  return temp;
  }*/





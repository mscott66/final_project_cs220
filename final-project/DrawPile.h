#ifndef DRAWPILE_H
#define DRAWPILE_H


#include "Card.h"
#include "Pile.h"
#include <vector>
#include <algorithm>
#include <cstdlib>
#include <string>

class DrawPile : public Pile {
 private:
  bool Rand = false;
 public:
  std::vector<Card> add(std::vector<Card> cards, bool trueOrFalse); //initialize draw pile
  std::vector<Card> shuffle(std::vector<Card> cards); //shuffles cards in deck; called from add if bool = true


 void display();
 DrawPile() : Pile(){}//constructor
  

  // Card draw(); (draw function that removes card and returns reference to card that is removed)

  std::string getRand() {
    if(Rand) {
      return "true";
    }
    return "false";
  }

};



#endif
